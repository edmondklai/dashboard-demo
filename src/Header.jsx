import React from 'react';

import Box from '@mui/material/Box';
import Divider from '@mui/material/Divider';
import './Header.css';

function Header() {
  return (
    <Box>
      <h1 className="Header">Civic Abundance</h1>
      <Divider></Divider>
    </Box>

  )
}

export default Header